package ts2

type TrainType struct {
	code string `json:"code"`
	description string `json:"description"`
	emergBraking float32 `json:"emergBraking"`
	length float32 `json:"length"`
	maxSpeed float32 `json:"maxSpeed"`
	stdAccel float32 `json:"stdAccel"`
	stdBraking float32 `json:"stdBraking"`
}

type trainTypes struct {
	id string
	data []TrainType
}