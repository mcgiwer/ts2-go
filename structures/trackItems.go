package ts2

type RoutesSetParams struct {}
type TrainNotPresentParams struct {}
type TrainPresentParams struct {}

type LineItem {
	conflictTiId []int `json:"conflictTiId"`
	maxSpeed float32 `json:"maxSpeed"`
	name string `json:"name"`
	nextTiId int `json:"nextTiId"`
	placeCode string `json:"placeCode"`
	previousTiId int `json:"previousTiId"`
	realLength float32 `json:"realLength"`
	tiId int `json:"tiId"`
	trackCode string `json:"trackCode"`
	x float32 `json:"x"`
	xf float32 `json:"xf"`
	y float32 `json:"y"`
	yf float32 `json:"yf"`
}

type SignalItem {
	conflictTiId []int `json:"conflictTiId"`
	maxSpeed float32 `json:"maxSpeed"`
	name string `json:"name"`
	nextTiId int `json:"nextTiId"`
	previousTiId int `json:"previousTiId"`
	reverse int `json:"reverse"`
	routesSetParams []RoutesSetParams `json:"routesSetParams"`
	signalType string `json:"signalType"`
	tiId int `json:"tiId"`
	trainNotPresentParams []TrainNotPresentParams `json:"trainNotPresentParams"`
	trainPresentParams []TrainPresentParams `json:"trainPresentParams"`
	x float32 `json:"x"`
	xn float32 `json:"xn"`
	y float32 `json:"y"`
	yn float32 `json:"yn"`
}

type PointsItem struct {
	conflictTiId []int `json:"conflictTiId"`
	maxSpeed float32 `json:"maxSpeed"`
	name string `json:"name"`
	nextTiId int `json:"nextTiId"`
	previousTiId int `json:"previousTiId"`
	reverseTiId int `json:"reverseTiId"`
	tiId int `json:"tiId"`
	x float32 `json:"x"`
	xf float32 `json:"xf"`
	xn float32 `json:"xn"`
	xr float32 `json:"xr"`
	y float32 `json:"y"`
	yf float32 `json:"yf"`
	yn float32 `json:"yn"`
	yr float32 `json:"yr"`
}

func main() {
	__type__ string `json:"__type__"`
	
	switch __type__ {
		case "LineItem":
			type trackItems struct {
				id string
				data []LineItem
			}
		case "SygnalItem":
			type signalType struct {
				id string
				data []SignalItem
			}
		case "PointsItem":
			type pointsItem struct {
				id string
				data []PointsItem
			}
	}
	
	
}