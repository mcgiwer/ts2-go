package ts2

type options struct {
	currentScore int `json:"currentScore"`
	currentTime string `json:"currentTime"`
	defaultDelayAtEntry int `json:"defaultDelayAtEntry"`
	defaultMaxSpeed float32 `json:"defaultMaxSpeed"`
	defaultMinimumStopTime string `json:"defaultMinimumStopTime"`
	defaultSignalVisibility int `json:"defaultSignalVisibility"`
	description string `json:"description"`
	timeFactor int `json:"timeFactor"`
	title string `json:"title"`
	trackCircuitBased int `json:"trackCircuitBased"`
	version float32 `json:"version"`
	warningSpeed float32 `json:"warningSpeed"`
}