package ts2

type TrainHead struct {
	positionOnTI float `json:"positionOnTI"`
	previousTI int "previousTI"`
	trackItem int "trackItem"`
}

type train struct {
	appearTime string: `json:"appearTime"`
	initialDelay string `json:"initialDelay"`
	initialSpeed float32 `json:"initialSpeed"`
	nextPlaceIndex int `json:"nextPlaceIndex"`
	serviceCode string `json:"serviceCode"`
	speed float32 `json:"speed"`
	status int `json:"status"`
	stoppedTime int `json:"stoppedTime"`
	trainHead []TrainHead `json:"trainHead"`
	trainId int `json:"trainId"`
	trainTypeCode string `json:"trainTypeCode"`
}