package ts2

type Lines struct {
	mustStop int `json:"mustStop"`
	placeCode string `json:"placeCode"`
	scheduledArrivalTime string `json:"scheduledArrivalTime"`
	scheduledDepartureTime string `json:"scheduledDepartureTime"`
	trackCode string `json:"trackCode"`
}

type ServiceID struct {
	autoReverse int `json:"autoReverse"`
	description string `json:"description"`
	lines []Lines `json:"lines"`
	nextServiceCode string `json:"nextServiceCode"`
	plannedTrainType string 'json:"plannedTrainType`
	serviceCode string `json:"serviceCode"`
}

type service struct {
	id []ServiceID
}