package server

import (
	"fmt"
	"net/http"
)

func main() {
	http.ListenAndServe(":8080", nil)
	http.HandleFunc("/", handler)
}

func handler(rs http.ResponseWriter, rq *http.Request){
	fmt.Println("Response Test")
	fmt.Fprintf(rs, "Test")
}